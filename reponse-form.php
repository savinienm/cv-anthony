<?php
//Comment valider les données en provenance de mon formulaire ?
if (!empty($_POST)){
//1 Les données obligatoires sont elles présentes ?
if( isset( $_POST["mail"]) && isset( $_POST["message"]) && isset( $_POST["prenom"]) && isset( $_POST["nom"]) && isset( $_POST["sujet"])){

    //2 Les données obligatoires sont-elles remplies ?
    if( !empty( $_POST["mail"]) && !empty( $_POST["message"]) && !empty( $_POST["prenom"]) && !empty( $_POST["nom"]) && !empty( $_POST["sujet"])){

        //3 Les données obligatoires ont-elles une valeur conforme à la forme attendue ?
        if( filter_var($_POST["mail"], FILTER_SANITIZE_EMAIL)){

            $_POST["mail"] = filter_var($_POST["mail"], FILTER_SANITIZE_EMAIL);

            if( filter_var($_POST["message"], FILTER_SANITIZE_FULL_SPECIAL_CHARS)){
                $_POST["message"] = filter_var($_POST["message"], FILTER_SANITIZE_FULL_SPECIAL_CHARS);

                if( filter_var($_POST["prenom"], FILTER_SANITIZE_STRING) && filter_var($_POST["nom"], FILTER_SANITIZE_STRING) && filter_var($_POST["sujet"], FILTER_SANITIZE_STRING)) {
                    $_POST["prenom"] = filter_var($_POST["prenom"], FILTER_SANITIZE_STRING);
                    $_POST["nom"] = filter_var($_POST["nom"], FILTER_SANITIZE_STRING);
                    $_POST["sujet"] = filter_var($_POST["sujet"], FILTER_SANITIZE_STRING);

                    //4 Les données optionnelles sont-elles présentes ? Les données optionnelles sont-elles remplies ? Les données optionnelles ont-elles une valeur conforme à la forme attendue ?                
                    if( isset($_POST["entreprise"])){
                        $_POST["entreprise"] = filter_var($_POST["entreprise"], FILTER_SANITIZE_STRING);
                    }else{$_POST["entreprise"] = "";
                    }

                    if( isset($_POST["fonction"])){
                        $_POST["fonction"] = filter_var($_POST["fonction"], FILTER_SANITIZE_STRING);
                    }else{$_POST["fonction"] = "";
                    }

                    if( isset($_POST["telephone"])){
                        $_POST["telephone"] = filter_var($_POST["telephone"], FILTER_SANITIZE_STRING);
                    }else{$_POST["telephone"] = "";
                    }

                    //5 afficher le mail et le message
                    printf('Bonjour %s, votre message a été envoyé et vous serez contacté à cette adresse %s, dans les plus bref délais !', $_POST["prenom"], $_POST["mail"]);

                    //6 utiliser la fonction mail() pour envoyer le message vers botre boîte mail
                    $to = "anthony.lioux@laposte.net";
                    $subject = $_POST["sujet"];
                    $message = 'Vous avez un nouveau message de ' . $_POST["prenom"] . ' ' . $_POST["nom"];
                    $message .= PHP_EOL . 'Contenu du message : ' . $_POST["message"];
                    if( $_POST["entreprise"]){
                        $message .= PHP_EOL . 'Entreprise : ' . $_POST["entreprise"];}
                    if( $_POST["fonction"]){
                        $message .= PHP_EOL . 'Fonction dans l\'entreprise : ' . $_POST["fonction"];}
                    if( $_POST["telephone"]){
                        $message .= PHP_EOL . 'Téléphone : ' . $_POST["telephone"];}
                    $headers = 'From: ' . $_POST["mail"]. PHP_EOL .
                    'Reply-To: ' .$_POST["mail"]. PHP_EOL .
                    'Content-Type: text/html; charset=UTF-8'. PHP_EOL .
                    'X-Mailer: PHP/' . phpversion() ;
                    mail($to, $subject, $message, $headers);


                }else {print("Fournissez des informations valides svp !");
                }

            }else {print("La forme de votre message n'est pas celle attendue !");
            }

        }else {print("Saisir une adresse email valide !");
        }

    }else {print("Il faut saisir les valeurs obligatoires !");
    }

}else {print("RENDS-MOI MES CODES!");
}
}